#!/bin/bash
nativefierBin="${PWD}/node_modules/.bin/nativefier"

if [ ! -f $nativefierBin ]; then
    npm install nativefier
fi

URL=$1
NAME=$2
PLATFORM=$3
LOGO=$4
DIR=$(dirname $LOGO)

$nativefierBin "$URL"\
    "$DIR" \
    --name "$NAME" \
    --platform "$PLATFORM" \
    --bounce \
    --counter \
    --honest \
    --tray true \
    --hide-window-frame \
    --disable-dev-tools \
    --title-bar-style "hidden" \
    --icon $LOGO

cp template.desktop $DIR/${NAME}.desktop

sed -i "s/%NAME%/$NAME/g" $DIR/${NAME}.desktop
sed -i "s#%BINARY%#$HOME/.local/bin/$NAME#g" $DIR/${NAME}.desktop
sed -i "s/%ICON%/$NAME/g" $DIR/${NAME}.desktop
