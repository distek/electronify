.PHONY: discord messages rocketchat

discord:
	-rm ~/.local/bin/Discord
	-rm -rf ~/.local/share/Discord-linux-x64
	./build.sh "https://canary.discord.com/app" "Discord" "linux" discord/logo.png
	# I can't figure out the correct way to do this with install
	# install -Dm744 discord/Discord-linux-x64/* ${HOME}/.local/share/Discord-linux-x64/
	cp -r discord/Discord-linux-x64 ${HOME}/.local/share/Discord-linux-x64
	ln -s ${HOME}/.local/share/Discord-linux-x64/Discord ${HOME}/.local/bin/Discord
	install -Dm644 discord/Discord.desktop ${HOME}/.local/share/applications/
	install -Dm644 discord/logo.png ${HOME}/.local/share/pixmaps/Discord.png
	install -Dm644 discord/logo.png ${HOME}/.local/share/icons/hicolor/512x512/apps/Discord.png

messages:
	-rm ~/.local/bin/Messages
	-rm -rf ~/.local/share/Messages-linux-x64
	./build.sh "https://messages.google.com/web" "Messages" "linux" messages/logo.png
	cp -r messages/Messages-linux-x64 ${HOME}/.local/share/Messages-linux-x64
	ln -s ${HOME}/.local/share/Messages-linux-x64/Messages ${HOME}/.local/bin/Messages
	install -Dm644 messages/Messages.desktop ${HOME}/.local/share/applications/
	install -Dm644 messages/logo.png ${HOME}/.local/share/pixmaps/Messages.png
	install -Dm644 messages/logo.png ${HOME}/.local/share/icons/hicolor/512x512/apps/Messages.png

rocketchat:
	-rm ~/.local/bin/RocketChat
	-rm -rf ~/.local/share/RocketChat-linux-x64
	./build.sh "https://ur.url.here" "RocketChat" "linux" rocketchat/logo.png
	cp -r rocketchat/RocketChat-linux-x64 ${HOME}/.local/share/RocketChat-linux-x64
	ln -s ${HOME}/.local/share/RocketChat-linux-x64/RocketChat ${HOME}/.local/bin/RocketChat
	install -Dm644 rocketchat/RocketChat.desktop ${HOME}/.local/share/applications/
	install -Dm644 rocketchat/logo.png ${HOME}/.local/share/pixmaps/RocketChat.png
	install -Dm644 rocketchat/logo.png ${HOME}/.local/share/icons/hicolor/512x512/apps/RocketChat.png
