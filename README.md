# Electronifier

Make whatever webapp an electron app

## Dependencies
* `npm`

* `electron`

## Build Instructions

`make discord`

`make messages`

etc.

## Notes
The rocketchat directive doesn't have a valid URL since typically you'd want to customize this to your specific server. So change that before attempting to build it, so:
```makefile
rocketchat:
    ...
	./build.sh "https://ur.url.here" "RocketChat" "linux" rocketchat/logo.png
    ...
```

change the `ur.url.here` to your desired server.
